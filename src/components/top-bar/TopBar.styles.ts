import styled from 'styled-components';
import { themeValues } from '@fibrbank/app-components';
import { up } from 'styled-breakpoints';

const pxToRem = themeValues.typography.pxToRem;
const palette = themeValues.palette;

export const Container = styled.div`
  background: ${palette.blue[300]};
`;

export const ContentWrapper = styled.div`
  padding: ${pxToRem(39)} 0;
  background: ${palette.white};

  ${up('sm')} {
    border-radius: 0px ${pxToRem(105)} 0px 0px;
  }
`;
