import { Logo } from '@fibrbank/app-components';

import * as S from './TopBar.styles';

export const AppTopBar = () => {
  return (
    <S.Container>
      <S.ContentWrapper>
        <Logo variant="dark" size="m" />
      </S.ContentWrapper>
    </S.Container>
  );
};
