import React, { ReactNode } from 'react';

import * as S from './GridLayout.styles';

export type TemplateProps = {
  children: ReactNode;
  sidebarComponent?: ReactNode;
};

export const GridLayout = ({ children, sidebarComponent }: TemplateProps) => {
  return (
    <S.Root>
      <S.Main>
        <S.GridContainer container spacing={0}>
          <S.LeftGrid item xs={12} md={8}>
            {children}
          </S.LeftGrid>
          <S.RightGrid item xs={12} md={4}>
            {sidebarComponent}
          </S.RightGrid>
        </S.GridContainer>
      </S.Main>
    </S.Root>
  );
};
