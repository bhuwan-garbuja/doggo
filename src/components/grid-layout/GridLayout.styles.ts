import styled, { css } from 'styled-components';
import { Grid } from '@fibrbank/app-components';
import { down } from 'styled-breakpoints';

export const Root = styled.div(
  ({ theme: { palette } }) => css`
    width: 100%;
    min-height: 100vh;
    background-color: ${palette.blue[300]};
    display: flex;
    flex-direction: column;
  `,
);

export const Main = styled.div(
  ({ theme: { palette } }) => css`
    background: ${`linear-gradient(to right, ${palette.white} 60%, ${palette.blue[300]} 40%)`};
    display: flex;
    flex: 1;
    z-index: 1;

    ${down('sm')} {
      background: ${palette.blue[300]};
    }
  `,
);

export const GridContainer = styled(Grid)(
  ({
    theme: {
      typography: { pxToRem },
    },
  }) => css`
    margin: 0 auto;
    max-width: ${pxToRem(1280)};
  `,
);

export const RightGrid = styled(Grid)(
  ({
    theme: {
      typography: { pxToRem },
      palette,
    },
  }) => css`
    background: ${palette.blue[300]};

    ${down('lg')} {
      padding: 0 ${pxToRem(10)};
    }
  `,
);

export const LeftGrid = styled(Grid)(
  ({
    theme: {
      typography: { pxToRem },
      palette,
    },
  }) => css`
    background: ${palette.white};

    ${down('sm')} {
      padding: 0 ${pxToRem(10)};
    }
  `,
);
