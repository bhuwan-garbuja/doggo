import 'styled-components';
import { ThemeSettingsType, themeValues } from '@fibrbank/app-components';

declare module 'styled-components' {
  export interface DefaultTheme extends ThemeSettingsType {
    palette: typeof themeValues.palette;
    typography: typeof themeValues.typography;
  }
}
