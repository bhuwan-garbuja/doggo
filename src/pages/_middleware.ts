import type { NextRequest } from 'next/server';

export function middleware(req: NextRequest) {
  const country = req.geo?.country || 'US';

  // console.log('req.geo::: ', req.geo);

  // If the request is from the blocked country,
  // send back a response with a status code
  if (req.geo || country === 'US') {
    // return new Response('Blocked for legal reasons', { status: 451 });
  }
}
