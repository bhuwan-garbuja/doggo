// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';

type QuickOffer = {
  id: string;
  loanAmount: number;
  loanTerms: number;
  repayment: 'Monthly';
  email: string;
  firstName: string;
  lastName: string;
  product: 'term-loan' | 'floc';
  isOfferUpdated: true;
  status: 'APPROVED';
  priority: 'primary' | 'secondary';
  affordabilityRoute: 'P&L' | 'Open Banking' | null;
  interestRate?: number;
  setupFee?: number;
  brokerCommission: number;
  totalSetupFee?: number;
  setupFeeAmount?: number;
  totalRepayment: number;
  monthlyRepayment: number;
  isPasswordCreated: false;
  companyName: string;
  companyId: string;
  declaredBrokerEmail: string;
  documentsSubmitted: false;
  tradingMonths: number;
};

type QuickOfferData = {
  quickOffer: QuickOffer;
  requestedAmount: number;
};

type RawData = {
  data: QuickOfferData[];
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<QuickOfferData>) {
  const server = 'https://dev-api.slice.farm';
  const userId = '2657501102219462045';
  const offer: RawData = await fetch(`${server}/dashboard/get-updated-quick-offers?userId=${userId}`, {
    headers: {
      Authorization: `userId ${userId}`,
      Accept: 'application/json',
    },
  }).then((res) => res.json());

  res.status(200).json(offer.data[0]);
}
