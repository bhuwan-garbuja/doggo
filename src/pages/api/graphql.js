import { ApolloServer, gql } from 'apollo-server-micro';

const typeDefs = gql`
  type Query {
    users: [User!]!
    company(id: String!): Company!
  }
  type User {
    name: String
  }
  type Company {
    id: String
    name: String
    status: String
    companyHouseNo: String
  }
`;

const resolvers = {
  Query: {
    users() {
      return [{ name: 'Max' }, { name: 'Manrika' }, { name: 'Pedro' }, { name: 'Alistair' }, { name: 'Rabi' }];
    },
    company(_parent, { id }) {
      return {
        id,
        name: 'HELLO LIMITED',
        status: 'SIGNUP_INCOMPLETE',
        companyHouseNo: '015728',
      };
    },
  },
};

const apolloServer = new ApolloServer({ typeDefs, resolvers });

const startServer = apolloServer.start();

export default async function handler(req, res) {
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Origin', 'https://studio.apollographql.com');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  if (req.method === 'OPTIONS') {
    res.end();
    return false;
  }

  await startServer;
  await apolloServer.createHandler({
    path: '/api/graphql',
  })(req, res);
}

export const config = {
  api: {
    bodyParser: false,
  },
};
