import '../styles/globals.css';
import { useEffect } from 'react';
import type { AppProps } from 'next/app';

import { GridLayout } from 'components/grid-layout';
import { AppTopBar } from 'components/top-bar';
import { MainTheme } from 'theme/main-theme';

function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement?.removeChild(jssStyles);
    }
  }, []);

  return (
    <MainTheme>
      <GridLayout>
        <AppTopBar />
        <Component {...pageProps} />
      </GridLayout>
    </MainTheme>
  );
}

export default MyApp;
