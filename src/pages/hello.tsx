import { Typography } from '@fibrbank/app-components';
import { GetServerSideProps } from 'next/types';

const Hello = ({ testing, token }) => {
  return (
    <Typography variant="body">
      Hello, {testing}, token: {token}
    </Typography>
  );
};

// SERVER - runs on the servER!!!!
export const getServerSideProps: GetServerSideProps<{ testing: string }> = async () => {
  // inject cookies - jwt token, CSRF token
  // console.log('helloo');
  const data = await fetch('https://api.github.com/users/apple').then((res) => res.json());

  return {
    props: {
      testing: 'props from server',
      token: data.node_id,
    },
  };
};

export default Hello;
