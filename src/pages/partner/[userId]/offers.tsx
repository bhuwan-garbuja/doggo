import type { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import { removeCookies, setCookies } from 'cookies-next';
import { Chip, Typography, InlineDropdown } from '@fibrbank/app-components';
import React from 'react';
import styled from 'styled-components';

const InlineTypography = styled(Typography)`
  display: inline;
`;

const basePath = 'test-application';

const Offers = ({ testing }: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const commissions = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5].map((value) => ({
    label: `${value}%`,
    value: `${value}`,
  }));

  const testFetch = async () => {
    await fetch(`/${basePath}/api/proxy`, { credentials: 'same-origin' }).then((res) => res.json());
  };

  return (
    <React.Fragment>
      <Chip label={testing} type="blue" />
      <br />

      <Typography
        variant="h3"
        fontWeight={600}
      >{`Great news! {Business name} could get £{100,000} over {36 months}`}</Typography>

      <Typography variant="body">The indicative offers below are based on your request for</Typography>
      <InlineTypography variant="body">{`£100,000 with a `}</InlineTypography>

      <InlineDropdown name="commission" value="1" maxItems={6} fontVariant="body" options={commissions} />
      <InlineTypography variant="body">{`commission`}</InlineTypography>

      <br />
      <button type="button" onClick={testFetch}>
        Click me and inspect
      </button>
    </React.Fragment>
  );
};

export const getServerSideProps: GetServerSideProps<{ testing: string }> = async ({ req, res }) => {
  removeCookies('anon-token');

  const jwt =
    'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkYtdG9iZlRoS0Nwd2lXLTBsNjhOMyJ9.eyJuaWNrbmFtZSI6ImJodXdhbi5nYXJidWphK2hlbGkiLCJuYW1lIjoiYmh1d2FuLmdhcmJ1amEraGVsaUBmaWJyLmNvbSIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci9iYzkyYThkZDE0MjhlMWNlZWE3Zjg0Zjc2NTI2NjA5NT9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRmJoLnBuZyIsInVwZGF0ZWRfYXQiOiIyMDIyLTAyLTE5VDIxOjA4OjIzLjA4NVoiLCJlbWFpbCI6ImJodXdhbi5nYXJidWphK2hlbGlAZmlici5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6Ly9hdXRoLmZpYnIuY29tLyIsInN1YiI6ImF1dGgwfDYxODU1NDM4ZDAyYWViMDA3MTM3MjZlOSIsImF1ZCI6IlBnMEJUSHVhZHliOEpTajdtalRxaVpVdDduTUJtck8zIiwiaWF0IjoxNjQ1MzA0OTAzLCJleHAiOjE2NDUzMTIxMDMsImF0X2hhc2giOiJTOGRfZEZHa2JrNl9OQ3ZUQjB2RUpRIiwibm9uY2UiOiI2RnM3b0lmY0JjR2g3WWxCRXVkZUVOWHZDUWRrUTBVYSJ9.it2PoP3mZqMPdHYJ0kgJ2pglIS2LkNykLCWvLWpxaoWhwn-IL3iBe3gOOXSeFQbKOApA605Z-7sRII7pNUktGOJRj8XTPu4_EansJP-hq5G1GyjGgR6Qvq3tpAUmv9FFbizlWfbVM-wOcgmP0W_lUkdXtlmp3SXD5yVCiuDqVbc4KAc20EnG6cV_RLxMT7uzFOcuAptV4-4E50Q_izMJxed2qTOFotaBWCgWmWLLOy14eaWh621ikw61nZakC4z56CSRZb0yX6vc5DEZhh0Y7E2rlIUxB4wDtpI8xoSrBa2lJ_R_eWSTUJm8liKXZa-jMWwJfWXGoVIqvPHLpfXd7g';
  setCookies('anon-token', jwt, { req, res, httpOnly: true, secure: true, maxAge: 3600 });

  return {
    props: {
      testing: 'props from server',
    },
  };
};

export default Offers;
