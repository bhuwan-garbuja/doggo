// /pages/_document_.js
import { ServerStyleSheets } from '@material-ui/core';
import Document, { DocumentContext, Head, Html, Main, NextScript } from 'next/document';
import React from 'react';
import { ServerStyleSheet } from 'styled-components';

// this is configured on `next.config.js` as basePath
const basePath = 'dev-application';

export default class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const materialSheets = new ServerStyleSheets();
    const styledComponentsSheet = new ServerStyleSheet();

    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) =>
            styledComponentsSheet.collectStyles(materialSheets.collect(<App {...props} />)),
        });
      const initialProps = await Document.getInitialProps(ctx);
      return {
        ...initialProps,
        locale: ctx?.locale,
        styles: (
          <React.Fragment>
            {initialProps.styles}
            {materialSheets.getStyleElement()}
            {styledComponentsSheet.getStyleElement()}
          </React.Fragment>
        ),
      };
    } finally {
      styledComponentsSheet.seal();
    }
  }
  render() {
    return (
      <Html lang="en" style={{ fontSize: '100%' }}>
        <Head>
          <link href={`/${basePath}/static/fonts/BeausiteClassicWeb-Semibold.woff2`} as="font" crossOrigin="" />
          <link href={`/${basePath}/static/fonts/BeausiteClassicWeb-Regular.woff2`} as="font" crossOrigin="" />
        </Head>
        <body style={{ fontSize: '1rem' }}>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
