// import { ThemeProvider } from 'styled-components';
import { createTheme } from 'styled-breakpoints';
import { breakpoints, ThemeProvider as UiThemeProvider } from '@fibrbank/app-components';
import { ReactNode } from 'react';

type ThemeProviderProps = {
  children: ReactNode;
};

export const themeValue = createTheme({
  xs: '319px',
  sm: `${breakpoints.sm}px`,
  md: `${breakpoints.md}px`,
  lg: `${breakpoints.lg}px`,
  xl: `${breakpoints.xl}px`,
});

export const MainTheme = ({ children }: ThemeProviderProps) => <UiThemeProvider>{children}</UiThemeProvider>;
